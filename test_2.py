import pytest
from math import *

def make_point(x,y):
    return (x,y)
def count_distance(a, b):
    x1, y1 = a
    x2, y2 = b
    return ((x1-x2)**2+(y1-y2)**2)**0.5
class point():
    def __init__(self,x,y):
        self.x=x
        self.y=y
    def __eq__(self,other):
        return self.x==other.x and self.y == other.y
    def __str__(self):
        return f"({self.x}, {self.y})"
    def distance(self,other):
        return ((self.x-other.x)**2+(self.y-other.y)**2)**0.5



def test_make_point():
    assert make_point(5,6)==(5,6)

def test_count_distance():
    a = make_point(6,8)
    b = make_point(0,0)
    assert count_distance(a, b)==10
    c = make_point(-1,-1)
    assert isclose(count_distance(a, c),11.40175425099138)

def test_point():
    assert point(6,5)!=point(6,8)
    assert point(5,5)==point(5,5)
    a=point(6,8)
    b=point(0,0)
    assert a.distance(b)==10
    assert str(point(4,5))=="(4, 5)"
