import pytest
from game import *

from math import *

def test_simple():
    assert True

@pytest.mark.xfail
def test_failing():
    assert False

def test_something():
    print('asd')

def test_sum():
    assert 2+2 == 4

@pytest.mark.xfail
def test_fact():
    assert factorial(5)==120
    assert factorial(6)==720
    assert factorial(7)==5040
    assert factorial(0)==1
    assert factorial(-1)==1

@pytest.mark.xfail
def test_sin():
    assert isclose(sin(pi/6),0.5)
    assert isclose(sin(pi/4),100)
    assert isclose(sin(pi/3),sqrt(3)/2)

def find_numbers(xs):
    ys=[]
    for i in xs:
        if i>0:
            ys.append(i)
    return ys

def test_find_numbers():
    assert find_numbers([])==[]
    assert find_numbers([0,1,-1])==[1]
    assert find_numbers([-1,-2,-3])==[]
def test_game():
    pass
def test_check():
    assert check(10,20)=="загаданное число больше введенного"
    assert check(20,10)=="загаданное число меньше введенного"
    assert check(20,20)=="победа"
def test_generate_number():
    for i in range(100):
        x=generate_number()
        assert x>0
        assert x<=100
from unittest.mock import patch
def test_generate_number_true():
    with patch("game.random.randint") as m:
        m.return_value=10
        assert generate_number()==10
def test_game():
    with patch("game.generate_number") as m1:
        m1.return_value=5
        with patch("game.input") as m2:
            m2.side_effect = ["1","2","3","4","5"]
            with patch("game.print") as m3:
                game()
                m3.assert_called_with("Победа")
